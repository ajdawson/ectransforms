# ectransforms

A Python interface to the ECMWF interpolation library EMOSLIB. ectransforms allows
simple grid transforms (spectral-to-spectral, spectral-to-grid) to be made on input
GRIB files. It is not a generic interface to EMOSLIB but provides a core set of
functionality.

## Installation

ectransforms requires that ECMWF's GRIB API and EMOSLIB be installed on your system.
In addition these libraries must have been compiled with the -fPIC flag. The 64-bit
real version of EMOSLIB should be used in order to accommodate high-resolution
transforms.

After downloading (and possibly extracting) the source code, change into the source
directory and run:

    $ python setup.py install --user

to install in your home directory.

If you have installed GRIB API and EMOSLIB in non-standard directories then the setup
script probably won't be able to find them unless you tell it where to look. In this
case do the following:

    $ python setup.py build_ext -L/path/to/emoslib:/path/to/grib_api install --user

where `/path/to/emoslib` and `path/to/grib_api` should be the paths where the libraries
can be found.
