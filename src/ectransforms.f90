!
! Purpose
! =======
!
! Apply grid transformations to GRIB fields using the ECMWF interpolation
! package EMOSLIB. The resulting transformations should be the same as those
! produced by the ECMWF MARS system.
!

! Copyright (c) 2012-2013 Andrew Dawson
!
! Permission is hereby granted, free of charge, to any person obtaining a copy
! of this software and associated documentation files (the "Software"), to deal
! in the Software without restriction, including without limitation the rights
! to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
! copies of the Software, and to permit persons to whom the Software is
! furnished to do so, subject to the following conditions:
!
! The above copyright notice and this permission notice shall be included in
! all copies or substantial portions of the Software.
!
! THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
! IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
! FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
! AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
! LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
! OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
! THE SOFTWARE.


subroutine autotruncation (ttype, gauss, lonlat, trunc)
    implicit none
!
! Input arguments:
    character(len=*),               intent(in) :: ttype
    integer,                        intent(in) :: gauss
    double precision, dimension(2), intent(in) :: lonlat
!
! Output arguments:
    integer, intent(out) :: trunc
!
! Purpose
! =======
!
! Wrapper for the EMOSLIB function AURESOL that determines the spectral
! truncation used for various grid transforms.
!
! Arguments
! =========
!
! ttype     (input) character(len=*)
!           The type of transformation to be applied:
!           = 'gaussian':  transform to a Gaussian grid;
!           = 'lonlat':    transform to a regulat longitude-latitude grid;
!           = 'reduced':   transform to a reduced Gaussian grid;
!
! gauss     (input) integer
!           The number of the Gaussian grid to transform to. Only used if
!           ttype = 'gaussian'.
!
! lonlat    (input) double precision, dimension(2)
!           The grid spacing in longitude and latitude for a regular
!           longitude-latitude grid. The first element is longitudinal grid
!           spacing, the second element is latitudinal grid spacing. Only
!           used if ttype = 'lonlat'.
!
! Author
! ======
!
! Andrew Dawson <dawson _at_ atm.ox.ac.uk>
!
    ! Local variable declarations.
    double precision :: gridsize

    ! External functions.
    integer :: auresol

    ! Convert Gaussian grid numbers to grid sizes.
    select case (ttype)
    case ('gaussian', 'reduced')
        gridsize = 90.0 / float(gauss)
        trunc = auresol(gridsize, gridsize)
    case ('lonlat')
        trunc = auresol(lonlat(1), lonlat(2))
    case default
        trunc = -1
    end select

    return
end subroutine autotruncation


subroutine interpopt (ttype, gauss, lonlat, qgauss, trunc, &
                      rotation, autotrunc, iret)
    implicit none
!
! Input arguments:
    character(len=*),               intent(in) :: ttype
    integer,                        intent(in) :: gauss
    double precision, dimension(2), intent(in) :: lonlat
    integer,                        intent(in) :: qgauss
    integer,                        intent(in) :: trunc
    double precision, dimension(2), intent(in) :: rotation
    logical,                        intent(in) :: autotrunc
!
! Output arguments:
    integer, intent(out) :: iret
!
! Purpose
! =======
!
! Set interpolation options for spectral/grid transformations.
!
! Arguments
! =========
!
! ttype     (input) character(len=*)
!           The type of transformation to be applied:
!           = 'gaussian':  transform to a Gaussian grid;
!           = 'lonlat':    transform to a regulat longitude-latitude grid;
!           = 'spectral':  transform to a different spectral truncation;
!           = 'rotated':   transform to rotated spectral coefficients(BROKEN);
!           = 'reduced':   transform to a reduced Gaussian grid;
!           = 'none':      just compute U and V.
!
! gauss     (input) integer
!           The number of the Gaussian grid to transform to. Only used if
!           ttype = 'gaussian'.
!
! lonlat    (input) double precision, dimension(2)
!           The grid spacing in longitude and latitude for a regular
!           longitude-latitude grid. The first element is longitudinal grid
!           spacing, the second element is latitudinal grid spacing. Only
!           used if ttype = 'lonlat'.
!
! qgauss    (input) integer
!           The number of the quasi-regular (reduced) Gaussian grid to
!           transform to. Values of 80 and 160 are supported by EMOSLIB. Only
!           used if ttype = 'reduced'
!
! trunc     (input) integer
!           The spectral trunctation to be applied. Only used if
!           ttype = 'spectral'.
!
! rotation  (input) double precision, dimension(2)
!           The latitude and longitude of the point that will be the South
!           pole of the rotated grid. Only used if ttype = 'rotated'.
!
! autotrunc (input) logical
!           Flag for application of automatic spectral truncation before other
!           transforms. Only influences the transforms 'gaussian', 'reduced'
!           and 'lonlat':
!           = .true.:  automatically truncate spectral coefficients;
!           = .false.: don't automatically truncate spectral coefficients.
!
! iret      (output) integer
!           Return status. A value of 0 indicates no errors. Non-zero values
!           indicate an error has occurred:
!           = 10  failed to define output Gaussian grid
!           = 11  failed to define output longitude-latitude grid
!           = 12  failed to define output spectral truncation
!           = 13  failed to define reduced Gaussian grid
!           = 14  failed to define rotation of spectral coefficients
!           = 16  failed to define automatic spectral truncation
!
! Author
! ======
!
! Andrew Dawson <dawson _at_ atm.ox.ac.uk>
!
    ! Local variable declarations.
    integer :: autotruncn

    ! Default variables used to call intout.
    integer :: intv
    double precision :: realv
    character(len=128) :: charv

    ! External EMOSLIB functions.
    integer :: intout

    if (ttype == 'gaussian') then
        ! A Gaussian grid is required.
        write (*, '(a)') 'interpopt (F90): setting gaussian grid'
        if (autotrunc) then
            call autotruncation (ttype, gauss, lonlat, autotruncn)
            iret = intout('truncation', autotruncn, realv, charv)
            if (iret /= 0) then
                iret = 16
                return
            end if
            write (*, '(a, I4)') &
                    'interpopt (F90): setting automatic truncation N=', &
                    autotruncn
        end if
        iret = intout('gaussian', gauss, realv, charv)
        if (iret /= 0) then
            iret = 10
            return
        end if
    else if (ttype == 'lonlat') then
        ! A regular latitude-longitude grid is required.
        write (*, '(a)') 'interpopt (F90): setting latitude-longitude grid'
        if (autotrunc) then
            call autotruncation (ttype, gauss, lonlat, autotruncn)
            iret = intout('truncation', autotruncn, realv, charv)
            if (iret /= 0) then
                iret = 16
                return
            end if
            write (*, '(a, I4)') &
                    'interpopt (F90): setting automatic truncation N=', &
                    autotruncn
        end if
        write (*, '(a)') 'setting grid'
        iret = intout('grid', intv, lonlat, charv)
        if (iret /= 0) then
            iret = 11
            return
        end if
    else if (ttype == 'spectral') then
        ! A spectral truncation is required.
        write (*, '(a)') 'interpopt (F90): setting truncation'
        iret = intout('truncation', trunc, realv, charv)
        if (iret /= 0) then
            iret = 12
            return
        end if
    else if (ttype == 'reduced') then
        ! A reduced Gaussian grid is required.
        write (*, '(a)') 'interpopt (F90): setting reduced gaussian grid'
        if (autotrunc) then
            call autotruncation (ttype, gauss, lonlat, autotruncn)
            iret = intout('truncation', autotruncn, realv, charv)
            if (iret /= 0) then
                iret = 16
                return
            end if
            write (*, '(a, I4)') &
                    'interpopt (F90): setting automatic truncation N=', &
                    autotruncn
        end if
        iret = intout('reduced', qgauss, realv, charv)
        if (iret /= 0) then
            iret = 13
            return
        end if
    else if (ttype == 'rotated') then
        ! Rotated spectral coefficients are requested.
        write (*, '(a)') 'interpopt (F90): setting rotated spectral coefficients'
        write (*, '("lat=", F4.1, " lon=", F4.1)') rotation(1), rotation(2)
        iret = intout('rotation', intv, rotation, charv)
        if (iret /= 0) then
            iret = 14
            return
        end if
    end if
    iret = 0
    return
end subroutine interpopt


subroutine transform (ifile, ofile, ttype, gauss, lonlat, qgauss, trunc, &
                      rotation, autotrunc, nprod, iret)
    implicit none
!
! Input arguments:
    character(len=*),               intent(in) :: ifile
    character(len=*),               intent(in) :: ofile
    character(len=*),               intent(in) :: ttype
    integer,                        intent(in) :: gauss
    double precision, dimension(2), intent(in) :: lonlat
    integer,                        intent(in) :: qgauss
    integer,                        intent(in) :: trunc
    double precision, dimension(2), intent(in) :: rotation
    logical,                        intent(in) :: autotrunc
!
! Output arguments:
    integer, intent(out) :: nprod
    integer, intent(out) :: iret
!
! Purpose
! =======
!
! Transform GRIB messages from one representation to another. This includes
! spectral truncation and rotation, spectral to gridpoint, and gridpoint to
! gridpoint transformations.
!
! Arguments
! =========
!
! ifile     (input) character(len=*)
!           Specifies the name of the GRIB file that contains the field(s)
!           to be transformed.
!
! ofile     (input) character(len=*)
!           Specifies the name of the GRIB file where the transformed output
!           will be stored.
!
! ttype     (input) character(len=*)
!           The type of transformation to be applied:
!           = 'gaussian':  transform to a Gaussian grid;
!           = 'reduced':   transform to a reduced Gaussian grid;
!           = 'lonlat':    transform to a regulat longitude-latitude grid;
!           = 'spectral':  transform to a different spectral truncation;
!           = 'rotated':   transform to rotated spectral coefficients.
!
! gauss     (input) integer
!           The number of the Gaussian grid to transform to. Only used if
!           ttype = 'gaussian'.
!
! lonlat    (input) double precision, dimension(2)
!           The grid spacing in longitude and latitude for a regular
!           longitude-latitude grid. The first element is longitudinal grid
!           spacing, the second element is latitudinal grid spacing. Only
!           used if ttype = 'lonlat'.
!
! qgauss    (input) integer
!           The number of the quasi-regular (reduced) Gaussian grid to
!           transform to. Values of 80 and 160 are supported by EMOSLIB. Only
!           used if ttype = 'reduced'
!
! trunc     (input) integer
!           The spectral trunctation to be applied. Only used if
!           ttype = 'spectral'.
!
! rotation  (input) double precision, dimension(2)
!           The latitude and longitude of the point that will be the South
!           pole of the rotated grid. Only used if ttype = 'rotated'.
!
! autotrunc (input) logical
!           Flag for application of automatic spectral truncation before other
!           transforms. Only influences the transforms 'gaussian', 'reduced'
!           and 'lonlat':
!           = .true.:  automatically truncate spectral coefficients;
!           = .false.: don't automatically truncate spectral coefficients.
!
! nprod     (output) integer
!           The number of products (GRIB records/messages) that were
!           interpolated.
!
! iret      (output) integer
!           Return status. A value of 0 indicates no errors. Non-zero values
!           indicate an error has occurred:
!           = 10  failed to define output Gaussian grid
!           = 11  failed to define output longitude-latitude grid
!           = 12  failed to define output spectral truncation
!           = 13  failed to define reduced Gaussian grid
!           = 14  failed to define rotation of spectral coefficients
!           = 15  interpolation routine failed
!           = 16  failed to define automatic spectral truncation
!           = 20  input GRIB file does not exist or cannot be opened
!           = 21  input GRIB file name is invalid
!           = 22  output GRIB file does not exist or cannot be opened
!           = 23  output GRIB file name is invalid
!           = 24  error in input file handling (e.g., truncated product)
!           = 25  insufficient space to store a whole product
!           = 26  error writing to the output GRIB file
!           = 99  internal error, please report
!
! Notes
! =====
!
! If receiving error 25, change the value of jpgrib to something larger. The
! maximum size of a readable product in bytes is jpgrib*jpbytes. The value of
! jpbytes is system dependent.
!
! Author
! ======
!
! Andrew Dawson <dawson _at_ atm.ox.ac.uk>
!
    ! Values for reading/writing GRIB files. jpgrib is the size of the integer
    ! arrays used to store GRIB fields. Here we make sure jpbytes matches the
    ! size of the integers we use in these arrays. It doesn't really matter
    ! what is used provided it matches the type of integer. The intrinsic
    ! 'sizeof' can be used to work out how many bytes is used to store an
    ! integer of a given kind. 
    integer, parameter :: jpgrib = 4000000
    integer, parameter :: jpbytes = 8

    ! GRIB fields, big enough to hold a large field. I think 32,000,000 bytes
    ! should be large enough for any single field, although this may change in
    ! the future.
    integer(kind=8), dimension(jpgrib) :: ingrib, newfld

    ! IO units for reading/writing GRIB files, these are not Fortran IO units
    ! but GRIB IO units, they are not interchangeable.
    integer :: iunit, ounit

    ! Additional variables used when reading and writing GRIB files.
    integer(kind=8) :: irec, inlen, newlen

    ! Define the return types of the EMOSLIB functions used.
    integer :: intf2

    ! A variable used to determine if any fields were interpolated yet.
    logical :: output_ready
    output_ready = .false.

    ! Set the interpolation options appropriate for the requested
    ! transformation.
    call interpopt (ttype, gauss, lonlat, qgauss, trunc, rotation, &
            autotrunc, iret)
    if (iret /= 0) then
        return
    end if

    ! Open the input file, this is just a plain binary file, we will only be
    ! reading and writing GRIB messages using the plain-binary API.
    call pbopen (iunit, ifile, 'r', iret)
    if (iret == -1) then
        ! File cannot be opened.
        iret = 20
        return
    else if (iret == -2) then
        ! File name is invalid.
        iret = 21
        return
    else if (iret == -3) then
        ! Error code -3 is for invalid mode, this should not occur in this
        ! routine.
        iret = 99
        return
    end if

    ! Loop over all the messages in the input file, interpolating and writing
    ! them to the output file.
    nprod = 0
    do while (.true.)
        
        ! Read a GRIB message from the input file.
        call pbgrib (iunit, ingrib, jpgrib * jpbytes, irec, iret)
        ! If there are no more messages to read -1 will be returned. In this
        ! case exit from the loop.
        if (iret == -1) exit
        ! If any other value than -1 or 0 are returned then an error has
        ! occurred, return with the exit status of the error.
        if (iret == -2) then
            call pbclose(iunit, iret)
            if (output_ready) then 
                call pbclose(ounit, iret)
            end if
            iret = 24
            return
        else if (iret == -3) then
            call pbclose(iunit, iret)
            if (output_ready) then 
                call pbclose(ounit, iret)
            end if
            iret = 25
            return
        end if

        ! Interpolate the GRIB message. intf2 is used so that both GRIB1 and
        ! GRIB2 may be used.
        newlen = jpgrib * jpbytes
        inlen = irec       ! length in bytes as returned by pbgrib
        iret = intf2(ingrib, inlen, newfld, newlen)
        ! If interpolation is unsuccessful a non-zero return value is
        ! returned.
        if (iret /= 0) then
            call pbclose(iunit, iret)
            if (output_ready) then 
                call pbclose(ounit, iret)
            end if
            iret = 15
            return
        end if
        ! If the output field is the same as the input field iret will be zero
        ! but the output length will also be zero. In this case there is no
        ! output to write.
        if (newlen /= 0) then

            ! Check if an output file has been opened already.
            if (.not. output_ready) then
                ! Open the output file. Like the input file, this is a plain
                ! binary file.
                call pbopen (ounit, ofile, 'w', iret)
                if (iret == -1) then
                    ! File cannot be opened, try to close the input file
                    ! before returning.
                    call pbclose(iunit, iret)
                    iret = 22
                    return
                else if (iret == -2) then
                    ! File name is invalid, try to close the input file before
                    ! returning.
                    call pbclose(iunit, iret)
                    iret = 23
                    return
                else if (iret == -3) then
                    ! Error code -3 is for invalid mode, this should not occur
                    ! in this routine.
                    call pbclose(iunit, iret)
                    iret = 99
                    return
                end if
                ! Mark the output file as ready.
                output_ready = .true.
            end if

            ! Write the interpolated field to the output GRIB file. The
            ! variable newlen stores the size of the interpolated field in
            ! bytes. Since pbwrite requires the length in bytes also newlen is
            ! passed straight in, rather than being multiplied by jpbytes as
            ! in the documentation. Doing so pads the fields unnecessarily
            ! causing large output files.
            call pbwrite (ounit, newfld, newlen, iret)
            if (iret == -1) then
                call pbclose(iunit, iret)
                call pbclose(ounit, iret)
                iret = 26
                return
            end if

            ! Increment the counter of the number of GRIB messages worked on.
            nprod = nprod + 1

        end if
        
    end do

    ! Close the input and output GRIB files. We will check if the files were
    ! closed correctly, ensuring that we have attempted to close both of them.
    call pbclose (iunit, iret)
    if (output_ready) then 
        call pbclose (ounit, iret)
    end if

    return
end subroutine transform


subroutine transformuv (vrfile, dvfile, ufile, vfile, ttype, gauss, lonlat, &
                        qgauss, trunc, rotation, autotrunc, nprod, iret)
    implicit none
!
! Input arguments:
    character(len=*),               intent(in) :: vrfile
    character(len=*),               intent(in) :: dvfile
    character(len=*),               intent(in) :: ufile
    character(len=*),               intent(in) :: vfile
    character(len=*),               intent(in) :: ttype
    integer,                        intent(in) :: gauss
    double precision, dimension(2), intent(in) :: lonlat
    integer,                        intent(in) :: qgauss
    integer,                        intent(in) :: trunc
    double precision, dimension(2), intent(in) :: rotation
    logical,                        intent(in) :: autotrunc
!
! Output arguments:
    integer, intent(out) :: nprod
    integer, intent(out) :: iret
!
! Purpose
! =======
!
! Transform vorticity and divergence (spectral coefficients) to U and V wind
! components. The wind components may be spectral coefficients (including
! truncated coefficients), or gridded.
!
! Arguments
! =========
!
! vrfile    (input) character(len=*)
!           Specifies the name of the GRIB file that contains the spectral
!           coefficients of vorticity.
!
! dvfile    (input) character(len=*)
!           Specifies the name of the GRIB file that contains the spectral
!           coefficients of divergence.
!
! ufile     (input) character(len=*)
!           Specifies the name of the GRIB file where the transformed
!           eastward wind component will be stored.
!
! vfile     (input) character(len=*)
!           Specifies the name of the GRIB file where the transformed
!           northward wind component will be stored.
!
! ttype     (input) character(len=*)
!           The type of transformation to be applied:
!           = 'gaussian':  transform to a Gaussian grid;
!           = 'lonlat':    transform to a regulat longitude-latitude grid;
!           = 'spectral':  transform to a different spectral truncation;
!           = 'rotated':   transform to rotated spectral coefficients(BROKEN);
!           = 'reduced':   transform to a reduced Gaussian grid;
!           = 'none':      just compute U and V.
!
! gauss     (input) integer
!           The number of the Gaussian grid to transform to. Only used if
!           ttype = 'gaussian'.
!
! lonlat    (input) double precision, dimension(2)
!           The grid spacing in longitude and latitude for a regular
!           longitude-latitude grid. The first element is longitudinal grid
!           spacing, the second element is latitudinal grid spacing. Only
!           used if ttype = 'lonlat'.
!
! qgauss    (input) integer
!           The number of the quasi-regular (reduced) Gaussian grid to
!           transform to. Values of 80 and 160 are supported by EMOSLIB. Only
!           used if ttype = 'reduced'
!
! trunc     (input) integer
!           The spectral trunctation to be applied. Only used if
!           ttype = 'spectral'.
!
! rotation  (input) double precision, dimension(2)
!           The latitude and longitude of the point that will be the South
!           pole of the rotated grid. Only used if ttype = 'rotated'.
!
! autotrunc (input) logical
!           Flag for application of automatic spectral truncation before other
!           transforms. Only influences the transforms 'gaussian', 'reduced'
!           and 'lonlat':
!           = .true.:  automatically truncate spectral coefficients;
!           = .false.: don't automatically truncate spectral coefficients.
!
! nprod     (output) integer
!           The number of products (GRIB records/messages) that were
!           interpolated.
!
! iret      (output) integer
!           Return status. A value of 0 indicates no errors. Non-zero values
!           indicate an error has occurred:
!           = 10  failed to define output Gaussian grid
!           = 11  failed to define output longitude-latitude grid
!           = 12  failed to define output spectral truncation
!           = 13  failed to define reduced Gaussian grid
!           = 14  failed to define rotation of spectral coefficients
!           = 15  interpolation routine failed
!           = 16  failed to define automatic spectral truncation
!           = 24  error in input file handling (e.g., truncated product)
!           = 25  insufficient space to store a whole product
!           = 26  error writing to the output GRIB file
!           = 30  vorticity GRIB file does not exist or cannot be opened
!           = 31  vorticity GRIB file name is invalid
!           = 32  divergence GRIB file does not exist or cannot be opened
!           = 33  divergence GRIB file name is invalid
!           = 34  U GRIB file does not exist or cannot be opened
!           = 35  U GRIB file name is invalid
!           = 36  V GRIB file does not exist or cannot be opened
!           = 37  V GRIB file name is invalid
!           = 38  error writing to the output U file
!           = 39  error writing to the output V file
!           = 99  internal error, please report
!
! Notes
! =====
!
! If receiving error 25, change the value of jpgrib to something larger. The
! maximum size of a readable product in bytes is jpgrib*jpbytes. The value of
! jpbytes is system dependent.
!
! Author
! ======
!
! Andrew Dawson <dawson _at_ atm.ox.ac.uk>
!
    ! Values for reading/writing GRIB files. jpgrib is the size of the integer
    ! arrays used to store GRIB fields. Here we make sure jpbytes matches the
    ! size of the integers we use in these arrays. It doesn't really matter
    ! what is used provided it matches the type of integer. The intrinsic
    ! 'sizeof' can be used to work out how many bytes is used to store an
    ! integer of a given kind. 
    integer, parameter :: jpgrib = 4000000
    integer, parameter :: jpbytes = 8

    ! GRIB fields, big enough to hold a large field. I think 32,000,000 bytes
    ! should be large enough for any single field, although this may change in
    ! the future.
    integer(kind=8), dimension(jpgrib) :: ivrgrib, idvgrib, iugrib, ivgrib

    ! IO units for reading/writing GRIB files, these are not Fortran IO units
    ! but GRIB IO units, they are not interchangeable.
    integer :: vrunit, dvunit, uunit, vunit

    ! Additional variables used when reading and writing GRIB files.
    integer(kind=8) :: irec, inlen, newlen

    ! Define the return types of the EMOSLIB functions used.
    integer :: intuvp2

    ! A variable used to determine if any fields were interpolated yet.
    logical :: output_ready
    output_ready = .false.

    ! Set the interpolation options appropriate for the requested
    ! transformation.
    call interpopt (ttype, gauss, lonlat, qgauss, trunc, rotation, &
            autotrunc, iret)
    if (iret /= 0) then
        return
    end if

    ! Open the input files, these are just a plain binary file, we will only
    ! be reading and writing GRIB messages using the plain-binary API.
    call pbopen (vrunit, vrfile, 'r', iret)
    if (iret == -1) then
        ! File cannot be opened.
        iret = 30
        return
    else if (iret == -2) then
        ! File name is invalid.
        iret = 31
        return
    else if (iret == -3) then
        ! Error code -3 is for invalid mode, this should not occur in this
        ! routine.
        iret = 99
        return
    end if
    call pbopen (dvunit, dvfile, 'r', iret)
    if (iret == -1) then
        ! File cannot be opened.
        iret = 32
        return
    else if (iret == -2) then
        ! File name is invalid.
        iret = 33
        return
    else if (iret == -3) then
        ! Error code -3 is for invalid mode, this should not occur in this
        ! routine.
        iret = 99
        return
    end if

    ! Loop over all the messages in the input files, interpolating and writing
    ! them to the output files.
    nprod = 0
    do while (.true.)

        ! Read a GRIB message from the input vorticity file.
        call pbgrib (vrunit, ivrgrib, jpgrib * jpbytes, irec, iret)
        ! If there are no more messages to read -1 will be returned. In this
        ! case exit from the loop.
        if (iret == -1) exit
        ! If any other value than -1 or 0 are returned then an error has
        ! occurred, return with the exit status of the error.
        if (iret == -2) then
            call pbclose (vrunit, iret)
            call pbclose (dvunit)
            if (output_ready) then 
                call pbclose (uunit, iret)
                call pbclose (vunit, iret)
            end if
            iret = 24
            return
        else if (iret == -3) then
            call pbclose (vrunit, iret)
            call pbclose (dvunit, iret)
            if (output_ready) then 
                call pbclose (uunit, iret)
                call pbclose (vunit, iret)
            end if
            iret = 25
            return
        end if
        ! Read a GRIB message from the input divergence file.
        call pbgrib (dvunit, idvgrib, jpgrib * jpbytes, irec, iret)
        ! If there are no more messages to read -1 will be returned. In this
        ! case exit from the loop.
        if (iret == -1) exit
        ! If any other value than -1 or 0 are returned then an error has
        ! occurred, return with the exit status of the error.
        if (iret == -2) then
            call pbclose (vrunit, iret)
            call pbclose (dvunit, iret)
            if (output_ready) then 
                call pbclose (uunit, iret)
                call pbclose (vunit, iret)
            end if
            iret = 24
            return
        else if (iret == -3) then
            call pbclose (vrunit, iret)
            call pbclose (dvunit, iret)
            if (output_ready) then 
                call pbclose (uunit, iret)
                call pbclose (vunit, iret)
            end if
            iret = 25
            return
        end if

        ! Compute U and V from vorticity and divergence.
        newlen = jpgrib * jpbytes
        inlen = irec
        iret = intuvp2(ivrgrib, idvgrib, inlen, iugrib, ivgrib, newlen)
        ! If interpolation is unsuccessful a non-zero return value is
        ! returned.
        if (iret /= 0) then
            call pbclose (vrunit, iret)
            call pbclose (dvunit, iret)
            if (output_ready) then 
                call pbclose (uunit, iret)
                call pbclose (vunit, iret)
            end if
            iret = 15
            return
        end if
        ! If the output field is the same as the input field iret will be zero
        ! but the output length will also be zero. In this case there is no
        ! output to write.
        if (newlen /= 0) then

            ! Check if output files has been opened already.
            if (.not. output_ready) then
                ! Open the output files. Like the input files, these are plain
                ! binary files.
                call pbopen (uunit, ufile, 'w', iret)
                if (iret == -1) then
                    ! File cannot be opened, try to close the input file
                    ! before returning.
                    call pbclose(vrunit, iret)
                    call pbclose(dvunit, iret)
                    iret = 34
                    return
                else if (iret == -2) then
                    ! File name is invalid, try to close the input file before
                    ! returning.
                    call pbclose(vrunit, iret)
                    call pbclose(dvunit, iret)
                    iret = 35
                    return
                else if (iret == -3) then
                    ! Error code -3 is for invalid mode, this should not occur
                    ! in this routine.
                    call pbclose(vrunit, iret)
                    call pbclose(dvunit, iret)
                    iret = 99
                    return
                end if
                call pbopen (vunit, vfile, 'w', iret)
                if (iret == -1) then
                    ! File cannot be opened, try to close the input file
                    ! before returning.
                    call pbclose(vrunit, iret)
                    call pbclose(dvunit, iret)
                    call pbclose(uunit, iret)
                    iret = 36
                    return
                else if (iret == -2) then
                    ! File name is invalid, try to close the input file before
                    ! returning.
                    call pbclose(vrunit, iret)
                    call pbclose(dvunit, iret)
                    call pbclose(uunit, iret)
                    iret = 37
                    return
                else if (iret == -3) then
                    ! Error code -3 is for invalid mode, this should not occur
                    ! in this routine.
                    call pbclose(vrunit, iret)
                    call pbclose(dvunit, iret)
                    call pbclose(uunit, iret)
                    iret = 99
                    return
                end if
                ! Mark the output file as ready.
                output_ready = .true.
            end if

            ! Write the interpolated fields to the output GRIB file. The
            ! variable newlen stores the size of the interpolated field in
            ! bytes. Since pbwrite requires the length in bytes also newlen is
            ! passed straight in, rather than being multiplied by jpbytes as
            ! in the documentation. Doing so pads the fields unnecessarily
            ! causing large output files.
            call pbwrite (uunit, iugrib, newlen, iret)
            if (iret == -1) then
                call pbclose(vrunit, iret)
                call pbclose(dvunit, iret)
                call pbclose(uunit, iret)
                call pbclose(vunit, iret)
                iret = 38
                return
            end if
            call pbwrite (vunit, ivgrib, newlen, iret)
            if (iret == -1) then
                call pbclose(vrunit, iret)
                call pbclose(dvunit, iret)
                call pbclose(uunit, iret)
                call pbclose(vunit, iret)
                iret = 39
                return
            end if

            ! Increment the counter of the number of GRIB messages worked on.
            nprod = nprod + 1

        end if

    end do

    ! Close the input and output GRIB files.
    call pbclose (vrunit, iret)
    call pbclose (dvunit, iret)
    if (output_ready) then
        call pbclose (uunit, iret)
        call pbclose (vunit, iret)
    endif

    return
end subroutine transformuv
