"""Build and install ectransforms."""
# Copyright (c) 2012-2013 Andrew Dawson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from numpy.distutils.core import setup, Extension


# Extract the package version from the library initialization script.
for line in open('lib/ectransforms/__init__.py').readlines():
    if line.startswith('__version__'):
        exec(line.strip())

# The extension module must be linked against EMOSLIB and  the GRIB API.
# It is expected that the 64-bit real version of EMOSLIB is in use
# (-lemosR64) since only this version supports higher resolution
# transformations. Both the GRIB API and EMOSLIB must have been compiled with
# the -fPIC flag.
libraries = ['emosR64', 'grib_api_f90', 'grib_api', 'jasper', 'openjpeg']

fortran_ext = Extension(name='_ectransforms',
                        libraries=libraries,
                        sources=['src/ectransforms.pyf', 'src/ectransforms.f90'])

setup(name='ectransforms',
      version=__version__,
      description='Wrapper for ECMWF EMOSLIB transformations.',
      author='Andrew Dawson',
      author_email='dawson@atm.ox.ac.uk',
      ext_modules=[fortran_ext],
      packages=['ectransforms'],
      package_dir={'': 'lib'},
      scripts=['bin/transform.py', 'bin/transformuv.py'],
     )
