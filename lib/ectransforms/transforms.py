"""Functions for handling spatial transforms."""
# Copyright (c) 2012-2013 Andrew Dawson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
from __future__ import absolute_import
import os
import sys
import warnings

import numpy as np

from _ectransforms import transform as _transform
from _ectransforms import transformuv as _transformuv
from .iotools import silence
from .errors import TransformError


# Define a mapping between Fortran return codes and the errors they represent.
_error_codes = {
         0: None,
        10: 'interpolation: failed to define output Gaussian grid',
        11: 'interpolation: failed to define output longitude-latitude grid',
        12: 'interpolation: failed to define output spectral truncation',
        13: 'interpolation: failed to define output quasi-regular Gaussian grid',
        14: 'interpolation: failed to define output rotation',
        15: 'interpolation: interpolation routine failed',
        16: 'interpolation: failed to define automatic spectral truncation',
        20: 'pbio: input GRIB file does not exist or cannot be opened',
        21: 'pbio: input GRIB file name is invalid',
        22: 'pbio: output GRIB file does not exist or cannot be opened',
        23: 'pbio: output GRIB file name is invalid',
        24: 'pbio: error in input file handling (e.g., truncated product)',
        25: 'pbio: insufficient space to store a whole product',
        26: 'pbio: error writing to the output GRIB file',
        30: 'pbio: vorticity GRIB file does not exist or cannot be opened',
        31: 'pbio: vorticity GRIB file name is invalid',
        32: 'pbio: divergence GRIB file does not exist or cannot be opened',
        33: 'pbio: divergence GRIB file name is invalid',
        34: 'pbio: U GRIB file does not exist or cannot be opened',
        35: 'pbio: U GRIB file name is invalid',
        36: 'pbio: V GRIB file does not exist or cannot be opened',
        37: 'pbio: V GRIB file name is invalid',
        38: 'pbio: error writing to the output U GRIB file',
        39: 'pbio: error writing to the output V GRIB file',
        99: 'internal: unexpected internal error, please report',
        999:'not implemented',
}


_err_msg = '{} is an invalid {} specification'


def _check_transform_args(gaussian, lonlat, reduced, spectral,
        none_ok=False):
    kws = (gaussian, lonlat, reduced, spectral)
    present_kws = filter(lambda e: False if e is None else True, kws)
    if not present_kws:
        if not none_ok:
            raise ValueError('no transformation specified')
    if len(present_kws) > 1:
        raise ValueError('ambiguous transformation specified')


def _get_transform_args(gaussian, lonlat, reduced, spectral):
    ttype = 'none'
    gaussian_f = 0
    lonlat_f = np.array([0., 0.], dtype=np.float64)
    reduced_f = 0
    spectral_f = 0
    rotated_f = np.array([0., 0.], dtype=np.float64)

    if gaussian is not None:
        ttype = 'gaussian'
        gaussian_f = _set_integer_arg(gaussian, 'gaussian grid')
    elif lonlat is not None:
        ttype = 'lonlat'
        lonlat_f = _set_array_arg(lonlat, lonlat_f, 'regular grid')
    elif reduced is not None:
        ttype = 'reduced'
        reduced_f = _set_integer_arg(reduced, 'quasi-regular gaussian grid')
    elif spectral is not None:
        ttype = 'spectral'
        spectral_f = _set_integer_arg(spectral, 'spectral truncation')

    return ttype, gaussian_f, lonlat_f, reduced_f, spectral_f, rotated_f


def _set_integer_arg(value, emsg):
    try:
        iarg = int(value)
        assert iarg > 0
    except (AssertionError, ValueError), e:
        raise TransformError(_err_msg.format(repr(value), emsg))
    return iarg


def _set_array_arg(value, default, emsg):
    try:
        default[:] = value
    except ValueError:
        raise TransformError(_err_msg.format(repr(value), emsg))
    return default


def _check_status(status):
    if status != 0:
        try:
            msg = _error_codes[status]
        except KeyError:
            msg = 'unknown error code {}: please report'.format(repr(status))
        raise TransformError(msg)


def transform(ifile, ofile, gaussian=None, lonlat=None, reduced=None,
        spectral=None, autotruncate=False, verbose=False):
    """Transform scalar fields contained in GRIB messages.
    
    This function is for transforming scalar fields. For vector fields
    see :py:func:`transformuv`.

    **Arguments:**
    
    *ifile*
        Name of input GRIB file containing scalar fields.

    *ofile*
        Name of output GRIB file to contain transformed fields.

    **Transform arguments:**

    One of these arguments should be given to specify the required
    transformation. An error is raised for ambiguous transformations.

    *gaussian*
        Transform fields to a gaussian grid: specify the grid number of
        the gaussian grid to transform fields to (e.g., 80 for an N80
        grid with 160 latitudes and 320 longitudes).

    *lonlat*
        Transform fields to a regular longitude-latitude grid: specify
        either a single value for the grid spacing in longitude and
        latitude or a two-element sequence for the grid spacing in the
        longitude and latitude directions respectively.

    *reduced*
        Transform fields to a reduced gaussian grid: specify the grid
        number of the reduced gaussian grid to transform fields to
        (e.g., 80 for an N80 grid with 160 latitudes).

    *spectral*
        Transform spectral fields to a new truncation: specify the
        spectral truncation to apply to the fields (e.g., 63 for T63
        truncation).

    **Optional arguments:**
    
    *autotruncate*
        If *True* spectral fields will be automatically truncated to an
        appropriate spectral truncation before transformation to grid.
        If *False* then spectral fields are not truncated automatically
        before grid transforms. Defaults to *False*.

    *verbose*
        If *True* then information from the transform routines will be
        written to stdout. If *False* then no information is written to
        stdout.

    """
    try:
        kws = _check_transform_args(gaussian, lonlat, reduced, spectral,
                none_ok=False)
        ttype, gaussian_f, lonlat_f, reduced_f, spectral_f, rotated_f = \
                _get_transform_args(gaussian, lonlat, reduced, spectral)
    except ValueError, e:
        raise TransformError(e)
    with silence(active=not verbose):
        nproducts, status = _transform(ifile,
                                       ofile,
                                       ttype,
                                       gaussian_f,
                                       lonlat_f,
                                       reduced_f,
                                       spectral_f,
                                       rotated_f,
                                       autotruncate)
    _check_status(status)
    return nproducts


def transformuv(vrfile, dvfile, ufile, vfile, gaussian=None, lonlat=None,
        reduced=None, spectral=None, autotruncate=False, verbose=False):
    """
    Transform spectral vorticity and divergence fields to wind
    components.
    
    This function is for transforming vector fields. For scalar fields
    see :py:func:`transform`.

    **Arguments:**
    
    *vrfile*
        Name of input GRIB file containing spectral vorticity fields.

    *dvfile*
        Name of input GRIB file containing spectral divergence fields.

    *ufile*
        Name of output GRIB file to contain transformed zonal wind
        fields.

    *vfile*
        Name of output GRIB file to contain transformed meridional wind
        fields.

    **Transform arguments:**

    One of these arguments  may be given to specify the required
    transformation. If none is given then spectral coefficients of the
    wind components are computed.

    *gaussian*
        Transform wind components to a gaussian grid: specify the grid
        number of the gaussian grid to transform to (e.g., 80 for an N80
        grid with 160 latitudes and 320 longitudes).

    *lonlat*
        Transform wind components to a regular longitude-latitude grid:
        specify either a single value for the grid spacing in both
        longitude and latitude or a two-element sequence for the grid
        spacing in the longitude and latitude directions respectively.

    *reduced*
        Transform wind components to a reduced gaussian grid: specify
        the grid number of the reduced gaussian grid to transform to
        (e.g., 80 for an N80 grid with 160 latitudes).

    *spectral*
        Transform wind components to a new truncation: specify the
        spectral truncation to apply to the fields (e.g., 63 for T63
        truncation).

    **Optional arguments:**
    
    *autotruncate*
        If *True* spectral fields will be automatically truncated to an
        appropriate spectral truncation before transformation to grid.
        If *False* then spectral fields are not truncated automatically
        before grid transforms. Defaults to *False*.

    *verbose*
        If *True* then information from the transform routines will be
        written to stdout. If *False* then no information is written to
        stdout.

    """
    try:
        kws = _check_transform_args(gaussian, lonlat, reduced, spectral,
                none_ok=True)
        ttype, gaussian_f, lonlat_f, reduced_f, spectral_f, rotated_f = \
                _get_transform_args(gaussian, lonlat, reduced, spectral)
    except ValueError, e:
        raise TransformError(e)
    with silence(active=not verbose):
        nproducts, status = _transformuv(vrfile,
                                         dvfile,
                                         ufile,
                                         vfile,
                                         ttype,
                                         gaussian_f,
                                         lonlat_f,
                                         reduced_f,
                                         spectral_f,
                                         rotated_f,
                                         autotruncate)
    _check_status(status)
    return nproducts
