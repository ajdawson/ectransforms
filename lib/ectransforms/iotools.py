"""Tools for low-level handling of IO.

The 'silence' class contained within was originally written by Greg
Haskins and released under the MIT license. This version is slightly
modified to allow turning the context manager on and off and includes
a bug fix to prevent leaking of file descriptors. The original code
(now including the file descriptors fix) is available here:

    http://code.activestate.com/recipes/
        577564-context-manager-for-low-level-redirection-of-stdou/

"""
# Copyright (C) 2011-2012 Greg Haskins.
# Copyright (C) 2012 Andrew Dawson.
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be
# included in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
# EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
# IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
# CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
# TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
# SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
import os


class silence(object):
    """Context manager to re-direct stdout/stderr at a low level.

    This class can re-direct (by default to /dev/null) stout and/or
    stderr at a level low enough to work with extension modules where
    the i/o originates in C/Fortran code. Output from higher levels will
    also be re-directed, making this context manager suitable for use in
    pure Python code also.

    """

    def __init__(self, stdout=os.devnull, stderr=os.devnull, mode='w',
            active=True):
        """Initialize the context manager.

        Optional arguments:
        stdout -- Name of the file to re-direct stdout to. Default to
                '/dev/null'.
        stderr -- Name of the file to re-direct stderr to. Defaults to
                '/dev/null'.
        mode -- Mode to open the re-direct files in. Defaults to 'w'
                (write, clobbering contents of the file if it exists).
        active -- If True, the context manager will re-direct output. If
                False, no re-direction will occur; this is useful for
                turning off the context manager dynamically. Defaults to
                True.

        """
        self.outfiles = stdout, stderr
        self.combine = (stdout == stderr)
        self.mode = mode
        self.active = active

    def __enter__(self):
        import sys
        self.sys = sys
        if not self.active:
            # Early exit if re-direction is turned off.
            return
        # Save the previous stdout and stderr
        self.saved_streams = sys.stdout, sys.stderr
        # Get the file descriptors of the saved streams, these are the ones we
        # will want to change.
        self.fds = [s.fileno() for s in self.saved_streams]
        # Make duplicates of these file descriptors so they may be changed
        # and eventually restored to their original state.
        self.saved_fds = map(os.dup, self.fds)
        # Flush any pending output to the streams.
        for s in self.saved_streams:
            s.flush()
        # Open surrogate files to replace the saved streams.
        if self.combine:
            # If stdout and stderr are the same then we just need the same new
            # file twice.
            self.null_streams = [open(self.outfiles[0], self.mode, 0)] * 2
            if self.outfiles[0] != os.devnull:
                # disable buffering so output is merged immediately.
                sys.stdout, sys.stderr = map(os.fdopen, self.fds, ['w']*2, [0]*2)
        else:
            # If stdout and stderr are different then create a file for each.
            self.null_streams  = [open(f, self.mode, 0) for f in self.outfiles]
        # Get the file descriptors of the null files.
        self.null_fds = [s.fileno() for s in self.null_streams]
        # Overwrite the file descriptors.
        map(os.dup2, self.null_fds, self.fds)

    def __exit__(self, *args):
        sys = self.sys
        if not self.active:
            # Early exit if re-direction is turned off. Returning False
            # ensures that exceptions are not suppressed.
            return False
        # Flush any pending output.
        for s in self.saved_streams:
            s.flush()
        # Restore original file descriptors.
        map(os.dup2, self.saved_fds, self.fds)
        sys.stdout, sys.stderr = self.saved_streams
        # Close the null files.
        for s in self.null_streams:
            s.close()
        # Close the file descriptors for the saved streams. These will be
        # leaked otherwise.
        for fd in self.saved_fds:
            os.close(fd)
        # Returning False indicates that any exceptions encountered should not
        # be suppressed.
        return False


if __name__ == '__main__':
    pass

