#!/usr/bin/env python
"""
Grid transformations for data in GRIB files.

Supports only spectral->spectral, spectral->gridpoint, and
gridpoint->gridpoint transformations. All transformations are carried
out using the EMOSLIB interpolation library from ECMWF.

One or both of the -o or -u options must be given. The program will not
allow overwriting of an input file.

"""
import os
import sys
from argparse import ArgumentParser

from ectransforms.transforms import transform
from ectransforms.errors import TransformError
from ectransforms.binhelpers import get_transform_args, set_transform_clargs, \
        add_file_suffix


def get_output_name(input_name, args):
    """Return an output file name based on an input file name.

    The settings in args are applied to the input name.

    """
    output_name = input_name
    if args.suffix is not None:
        output_name = add_file_suffix(output_name, args.suffix[0])
    if args.output_directory is not None:
        output_name = os.path.join(args.output_directory[0],
                os.path.basename(output_name))
    return output_name


def main(argv=None):
    """Program entry point."""
    if argv is None:
        # Use command line arguments if none are passed in.
        argv = sys.argv
    # Create a command line option parser.
    ap = ArgumentParser(prog=os.path.basename(argv[0]),
            description=__doc__)
    # Add arguments for transform specifications:
    set_transform_clargs(ap)
    # Add arguments for program behaviour:
    ap.add_argument('-o', '--output-directory', nargs=1, metavar='outdir',
            help='directory to write output in')
    ap.add_argument('-u', '--suffix', nargs=1, metavar='suffix',
            help='suffix appended to output file names')
    ap.add_argument('-v', '--verbose', action='store_true',
            help='allow output from EMOSLIB routines to be printed')
    ap.add_argument('-n', '--dry-run', action='store_true',
            help='print what would be done without doing it')
    # Add positional arguments, the input GRIB files to transform.
    ap.add_argument('gribfiles', nargs='+', metavar='grib_file')
    try:
        # Parse the command line arguments.
        args = ap.parse_args(argv[1:])
        tname, targs = get_transform_args(args) # returns None for no transform
        targs.update({'autotruncate': True})
        if tname is None:
            ap.print_usage()
            return 0
        rval = 0
        for grbinfile in args.gribfiles:
            grboutfile = get_output_name(grbinfile, args)
            if grboutfile == grbinfile:
                print >> sys.stderr, 'warning: output file same as input'
                print >> sys.stderr, '    %s skipped' % grbinfile
                rval = 5
                continue
            try:
                if not args.dry_run:
                    n = transform(grbinfile, grboutfile, **targs)
                else:
                    n = -1
                print '{} -> {} ({})'.format(grbinfile, grboutfile, tname)
                print 'interpolated {0:d} products'.format(n)
            except TransformError, e:
                # Print a message if the transformation fails but carry on
                # with the other files.
                print >> sys.stderr, 'error: {}'.format(e)
                print >> sys.stderr, '    {} skipped'.format(grbinfile)
                rval = 5
        # Return indicating success or otherwise.
        return rval
    except TransformError, e:
        print >> sys.stderr, 'error: {}'.format(e)
        print >> sys.stderr, '   use -h or --help for help'
        return 1


if __name__ == '__main__':
    sys.exit(main())
