"""Helpers for applications written using ectransforms."""
# Copyright (c) 2012-2013 Andrew Dawson
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
# THE SOFTWARE.
import os


def add_file_suffix(filename, suffix):
    name, ext = os.path.splitext(filename)
    return '{}{}{}'.format(name, suffix, ext)


def set_transform_clargs(parser):
    transform_group = parser.add_argument_group('transform arguments')
    transform_group.add_argument('-g', '--gaussian', nargs=1, type=int,
            metavar='N', help='gaussian grid number')
    transform_group.add_argument('-q', '--qgaussian', nargs=1, type=int,
            metavar='N', help='quasi-gaussian grid number')
    transform_group.add_argument('-s', '--spectral', nargs=1, type=int,
            metavar='T', help='spectral truncation number')
    transform_group.add_argument('-l', '--lonlat', nargs=1, type=str,
            metavar='lon,lat', help='regular longitude-latitude grid')


def get_transform_args(args):
    """
    Get a dictionary of keyword arguments for the transform function
    from command line arguments.

    """
    # Make a list of all the transform specifications, if a specification
    # was not provided its value will be None.
    transform_specs = {'gaussian': args.gaussian, 'lonlat': args.lonlat,
            'reduced': args.qgaussian, 'spectral': args.spectral}
    # Make a list of all the transform specifications that were provided.
    present_transform_specs = filter(
        lambda s: False if transform_specs[s] is None else True,
        transform_specs)
    if not present_transform_specs:
        # Return None if no transform specification was provided.
        return 'none', {}
    if len(present_transform_specs) != 1:
        # If more than one transform specification was given then raise an
        # error.
        raise Usage('only one type of transformation is allowed')
    transform_name = present_transform_specs[0]
    if transform_name in ('gaussian', 'reduced', 'spectral'):
        # 'gaussian', 'reduced', and spectral are single values.
        transform_value = transform_specs[transform_name][0]
    else:
        # 'rotated' and 'lonlat' are lists.
        transform_value = \
            [float(v) for v in transform_specs[transform_name][0].split(',')]
    return transform_name, \
            {transform_name: transform_value, 'verbose': args.verbose}
