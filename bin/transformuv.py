#!/usr/bin/env python
"""
Transform spectral vorticity and divergence coefficients into wind
components.

Supports spectral->spectral and spectral->gridpoint transformations.
All transformations are carried out using the EMOSLIB interpolation
library from ECMWF.

"""
import os
import sys
from argparse import ArgumentParser

from ectransforms.transforms import transformuv
from ectransforms.errors import TransformError
from ectransforms.binhelpers import get_transform_args, set_transform_clargs


def main(argv=None):
    if argv is None:
        argv = sys.argv
    # Create a command line argument parser.
    ap = ArgumentParser(prog = os.path.basename(argv[0]),
            description=__doc__)
    # Add the transform specification arguments using a helper function.
    set_transform_clargs(ap)
    # Add arguments for program behaviour.
    ap.add_argument('-v', '--verbose', action='store_true',
            help='allow output from EMOSLIB routines to be printed')
    ap.add_argument('-n', '--dry-run', action='store_true',
            help='print what would be done without doing it')
    # Add positional arguments for input and output GRIB files.
    ap.add_argument('vrtgrib', nargs=1, metavar='vorticity_grib_file')
    ap.add_argument('divgrib', nargs=1, metavar='divergence_grib_file')
    ap.add_argument('uwndgrib', nargs=1, metavar='zonal_wind_grib_file')
    ap.add_argument('vwndgrib', nargs=1, metavar='meridional_wind_grib_file')
    try:
        # Parse the command line arguments.
        args = ap.parse_args(argv[1:])
        # Input and output GRIB file names.
        vrtgrib = args.vrtgrib[0]
        divgrib = args.divgrib[0]
        uwndgrib = args.uwndgrib[0]
        vwndgrib = args.vwndgrib[0]
        # Get transform arguments.
        tname, targs = get_transform_args(args)
        targs.update({'autotruncate': True})
        rval = 0
        try:
            if not args.dry_run:
                # Do the interpolation and record the number of products
                # interpolated.
                n = transformuv(vrtgrib, divgrib, uwndgrib, vwndgrib, **targs)
            else:
                # Dry run mode: return a dummy number of products.
                n = -1
            print '{},{} -> {},{} ({})'.format(vrtgrib, divgrib, uwndgrib,
                    vwndgrib, tname)
            print 'interpolated {0:d} products'.format(n)
        except TransformError, e:
            print >> sys.stderr, 'error: {}'.format(e)
            print >> sys.stderr, '    {},{} skipped'.format(vrtgrib, divgrib)
            rval = 5
            # Return indicating success or otherwise.
        return rval
    except TransformError, e:
        print >> sys.stderr, 'error: {}'.format(e)
        print >> sys.stderr, '    use -h or --help for help'
        return 1


if __name__ == '__main__':
    sys.exit(main())
